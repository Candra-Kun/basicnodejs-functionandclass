const Vehicle = require('./vehicle');
const Owner = require('./owner_vehicle');

function main() {
  let myVehicle = new Vehicle('D 1234 AZ', 'Black', 'Toyota', 'Car', 'Gasoline');
  let ownerData = new Owner('Slamet Setiadi', 'Jalan DI Pandjaitan,\nKecamatan Purwokerto Selatan, Kabupaten Banyumas');

  
  console.log('-----------------Owner information-----------------');
  console.log(`Name is ${ownerData.ownername}\nAddress at ${ownerData.address}\n`);
  console.log('----------------Vehicle information----------------');
  console.log(`License Number is ${myVehicle.licenseNumber}\nColor is ${myVehicle.color}`);
  console.log(`Manufacturer is ${myVehicle.manufacturer}\nCategory is ${myVehicle.category}\nFuel is ${myVehicle.fuel}`);

  myVehicle.drive(120)
  myVehicle.price(150000000)
}

main()
