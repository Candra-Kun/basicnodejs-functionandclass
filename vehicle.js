class Vehicle {
  constructor(licenseNumber, color, manufacturer, category, fuel){
    this.licenseNumber = licenseNumber;
    this.color = color;
    this.manufacturer = manufacturer;
    this.category = category;
    this.fuel = fuel;
  }

  
  drive(maxSpeed) {
    console.log('Maximum speed', maxSpeed, 'km/h');
  }

  price(value) {
    console.log('Price is RP', value,);
  }
}



module.exports = Vehicle;